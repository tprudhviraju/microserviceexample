package com.prudhvi.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableCaching
@EnableSwagger2
@SpringBootApplication
//@EnableCircuitBreaker
//@EnableScheduling
//@EnableSchedulerLock(defaultLockAtMostFor = "5m")
//@ComponentScan(basePackages = "com.prudhvi.customer")
//@EnableJpaRepositories(basePackages = "com.prudhvi.customer.repository")
//@EntityScan(basePackages = "com.prudhvi.customer.entity")
public class CustomerMicroServiceApplication extends AsyncConfigurerSupport{

	public static void main(String[] args) {
		//TimeZone.setDefault(TimeZone.getTimeZone("America/New_York"));
		SpringApplication.run(CustomerMicroServiceApplication.class, args);
	}
	

//	@Bean
//	public LockProvider lockProvider(DataSource dataSource) {
//		return new JdbcTemplateLockProvider(
//				JdbcTemplateLockProvider.Configuration.builder()
//				.withJdbcTemplate(new JdbcTemplate(dataSource))
//				.usingDbTime()
//				.build()
//				);
//	}
//	
//	@Bean
//	public RestTemplate restTemplate() {
//		return new RestTemplate();
//	}

}
