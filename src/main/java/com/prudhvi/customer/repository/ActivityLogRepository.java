package com.prudhvi.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prudhvi.customer.entity.ActivityLog;

@Repository
public interface ActivityLogRepository extends JpaRepository<ActivityLog,Long> {

}
