package com.prudhvi.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prudhvi.customer.entity.Institution;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Long> {

	Institution findByInstitutionCode(String institutionCode);
}
