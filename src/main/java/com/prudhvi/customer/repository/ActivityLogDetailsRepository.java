package com.prudhvi.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.prudhvi.customer.entity.ActivityLogDetails;

@Repository
public interface ActivityLogDetailsRepository extends JpaRepository<ActivityLogDetails, Long>,PagingAndSortingRepository<ActivityLogDetails, Long>{

}
