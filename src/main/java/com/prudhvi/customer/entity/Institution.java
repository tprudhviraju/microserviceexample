package com.prudhvi.customer.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Institution")
public class Institution implements Serializable{
	
	private static final long serialVersionUID = 5364297146267905524L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",length=5,updatable=false,nullable=false)
	private Integer id;
	
	@Column(name="institution_code",nullable=false,length=15)
	private String institutionCode;
	
	@Column(name="institution_name",nullable=false,length=150)
	private String institutionName;
	
	@Column(name="institution_type",length=2)
	private Integer institutionType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

}
