package com.prudhvi.customer.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Activity_Log_Details")
public class ActivityLogDetails implements Serializable {

	private static final long serialVersionUID = -6853518902480412513L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ALD_ID")
	private Long uniqueId;

	@Column(name="OLD_VALUE")
	private String oldValue;

	@Column(name="NEW_VALUE")
	private String newValue;

	@ManyToOne
	@JoinColumn(name ="AL_ID")
	private ActivityLog activityLog;

	public Long getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public ActivityLog getActivityLog() {
		return activityLog;
	}

	public void setActivityLog(ActivityLog activityLog) {
		this.activityLog = activityLog;
	}

	@Override
	public String toString() {
		return "ActivityLogDetails [uniqueId=" + uniqueId + ", oldValue=" + oldValue + ", newValue=" + newValue
				+ ", activityLog=" + activityLog + "]";
	}

}
