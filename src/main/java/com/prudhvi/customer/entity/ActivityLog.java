package com.prudhvi.customer.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Activity_Log")
public class ActivityLog implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="AL_ID")
	private Long uniqueId;
	
	@Column(name="BRANCH_ID")
	private String branchId;
	
	@Column(name="USER_ID")
	private String userId;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="AL_ID")
	private Set<ActivityLogDetails> activityLogDetails=new HashSet<>();

	public Long getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Set<ActivityLogDetails> getActivityLogDetails() {
		return activityLogDetails;
	}

	public void setActivityLogDetails(Set<ActivityLogDetails> activityLogDetails) {
		this.activityLogDetails = activityLogDetails;
	}

	@Override
	public String toString() {
		return "ActivityLog [uniqueId=" + uniqueId + ", branchId=" + branchId + ", userId=" + userId
				+ ", activityLogDetails=" + activityLogDetails + "]";
	}

}
